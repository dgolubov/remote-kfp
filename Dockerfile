FROM python:3.6

COPY auth-get-sso-cookie /opt/auth-get-sso-cookie
COPY krb5.conf /etc/krb5.conf
ENV DEBIAN_FRONTEND=noninteractive

RUN pip3 install --upgrade pip && pip3 install -r /opt/auth-get-sso-cookie/requirements.txt

WORKDIR /opt/auth-get-sso-cookie

RUN apt-get update && \
    apt-get -yqq install libpam-krb5 krb5-user && \
    python setup.py install

COPY remote_kfp.py /opt/remote_kfp.py