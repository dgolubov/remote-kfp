# auth-get-sso-cookie

This utility is a replacement for `cern-get-sso-cookie` for the new MALT SSO.

`auth-get-sso-cookie` acquires CERN Single Sign-On cookie using Kerberos credentials allowing for automated access to CERN SSO protected pages using tools alike wget, curl or similar. 

Additionally, this package includes another utility `auth-get-sso-token`, that gets a JWT access token for OpenID Connect applications using a public client. This utility also authenticates using Kerberos credentials.


## Install
Run the setuptools file included with the sources:

```
python setup.py install
```

Alternatively, you can install the Koji package `auth-get-sso-cookie`.


## Usage

You will need a valid Kerberos TGT to run the utility, e.g. run `kinit <user>` before the script.


## auth-get-sso-cookie

Use this tool to get a valid SSO and application cookie from a protected URL. This cookie will be valid for 10 hours.

**Warning:** Every time you get new cookies, this will start a new SSO session but it won't log off any other session. To avoid starting too many sessions, please reuse your cookies as much as possible while they are valid.

```
$ auth-get-sso-cookie --help
usage: auth-get-sso-cookie [-h] [-u URL] [-o OUTFILE] [--nocertverify]
                              [--verbose] [--debug]

Acquires the CERN Single Sign-On cookie using Kerberos credentials

optional arguments:
  -h, --help            show this help message and exit
  -u URL, --url URL     CERN SSO protected site URL to get cookie for.
  -o OUTFILE, --outfile OUTFILE
                        File to store the cookie for further usage
  --nocertverify        Disables peer certificate verification. Useful for
                        debugging/tests when peer host does have a self-signed
                        certificate for example.
  --verbose, -v         Provide more information on authentication process
  --debug, -vv          Provide detailed debugging information
```


```
auth-get-sso-cookie -u <url> -o <cookies_file>
```
Example: 
```
$ auth-get-sso-cookie -u https://openstack.cern.ch -o cookies.txt
$ curl -L -b cookies.txt https://openstack.cern.ch
```


## auth-get-sso-token

Use this tool to get a valid SSO token for a protected URL. The obtained token will be valid for 20 minutes.

```
$ auth-get-sso-token --help
usage: auth-get-sso-token [-h] [--url URL] [--clientid CLIENTID] [--nocertverify] [--verbose] [--debug]

Acquires a user token for a public client using Kerberos credentials

optional arguments:
  -h, --help            show this help message and exit
  --url URL, -u URL     Application or Redirect URL. Required for the OAuth request.
  --clientid CLIENTID, -c CLIENTID
                        Client ID of a public client
  --nocertverify        Disables peer certificate verification. Useful for debugging/tests when peer host does have a self-signed certificate for example.
  --verbose, -v         Provide more information on authentication process
  --debug, -vv          Provide detailed debugging information
```

Example:

```
$ TOKEN=$(./auth-get-sso-token -u http://localhost:5000 -c get-sso-token-test)
$ curl -X PUT "https://localhost:5000/api/foobar" -H  "authorization: Bearer $TOKEN" -d "{\"foo\": \"bar\"}"
```


## Limitations

Certificate credentials are not supported in the new SSO: you will get a warning message if you use the old options for certificates (`--cert`, `--key`, `--cacert`, `--capath`) or `--krb`. The behaviour of the tool will be exactly the same as if these options are not specified.


## Testing

Use unittest to test the `cern_sso` module. You will need a valid Kerberos TGT to run the tests.

```
python -m unittest test_cern_sso
```
